# device_view.py
#
# Copyright 2023 Gerben Droogers
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# SPDX-License-Identifier: GPL-3.0-or-later

import logging

from gi.repository import Adw, Gtk, GObject
from .activity_list_item import ActivityListItem
from .fit import Fit

@Gtk.Template(resource_path='/nl/g4d/Fitbuddy/device_view.ui')
class DeviceView(Gtk.Box):
    __gtype_name__ = 'DeviceView'

    __gsignals__ = {
        'activity-selected': (GObject.SignalFlags.RUN_FIRST, None, (object,)),
    }

    _fit = None

    _name = Gtk.Template.Child()
    _software_version = Gtk.Template.Child()
    _serial_number = Gtk.Template.Child()
    _activity_list = Gtk.Template.Child()

    def __init__(self, fit, **kwargs):
        super().__init__(**kwargs)
        self._fit = fit

        device = fit.device()

        self._name.set_label(device.manufacturer + " " + device.garmin_product)
        self._software_version.set_label("Software: " + str(device.software_version))
        self._serial_number.set_label("Serial: " + str(device.serial_number))

        for activity in fit.activities():
            box = ActivityListItem(activity)
            self._activity_list.append(box)

    @Gtk.Template.Callback()
    def on_row_activated(self, widget, row):
        self.emit('activity-selected',row.get_child().get_activity())

