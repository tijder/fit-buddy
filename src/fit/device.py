# device.py
#
# Copyright 2023 Gerben Droogers
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# SPDX-License-Identifier: GPL-3.0-or-later

class Device():
    garmin_product = None
    device_type = None
    manufacturer = None
    serial_number = None
    software_version = None

    def __init__(self, garmin_product=None, device_type=None, manufacturer=None, serial_number=None, software_version=None):
        self.garmin_product = garmin_product
        self.device_type = device_type
        self.manufacturer = manufacturer
        self.serial_number = serial_number
        self.software_version = software_version

    def __str__(self):
     return self.garmin_product

