# activity.py
#
# Copyright 2023 Gerben Droogers
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# SPDX-License-Identifier: GPL-3.0-or-later

from .activity_position import ActivityPosition

class Activity():
    sport_name = None
    sport_sport = None
    start_time = None
    start_position = None
    total_elapsed_time = None
    total_distance = None
    enhanced_avg_speed = None
    enhanced_max_speed = None
    avg_speed = None
    max_speed = None
    total_ascent = None
    total_descent = None
    avg_heart_rate = None
    max_heart_rate = None

    positions = []

    def __init__(self, messages):
        self.sport_name = messages['sport_mesgs'][0]['name']
        self.sport_sport = messages['sport_mesgs'][0]['sport']
        self.start_time = messages['session_mesgs'][0]['start_time']
        self.start_position = [messages['session_mesgs'][0]['start_position_lat'], messages['session_mesgs'][0]['start_position_long']]
        self.total_elapsed_time = messages['session_mesgs'][0]['total_elapsed_time']
        self.total_distance = messages['session_mesgs'][0]['total_distance']

    def add_position(self, position: ActivityPosition):
        self.positions.append(position)

    def get_gpx_track(self) -> str:
        string = """<?xml version="1.0"?>\n<gpx version="1.1">\n  <trk>\n    <name>GPX from FIT {}</name>\n    <trkseg>""".format(self.sport_name)
        for position in self.positions:
            string = string +  """\n      <trkpt lat="{}" lon="{}">\n        <ele>{}</ele>\n        <time>{}</time>\n      </trkpt>""".format(position.position[0], position.position[1], position.enhanced_altitude, position.timestamp.strftime('%Y-%m-%dT%H:%M:%SZ'))
        string = string + "\n    </trkseg>\n  </trk>\n</gpx>"
        return string

    def __str__(self):
     return self.sport_name
