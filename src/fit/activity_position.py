# activity_position.py
#
# Copyright 2023 Gerben Droogers
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# SPDX-License-Identifier: GPL-3.0-or-later

class ActivityPosition():
    timestamp = None
    position = None
    distance = None
    enhanced_speed = None
    enhanced_altitude = None
    heart_rate = None

    def __init__(self, timestamp=None, position=None, distance=None, enhanced_speed=None, enhanced_altitude=None, heart_rate=None):
        self.timestamp = timestamp
        self.position = position
        self.distance = distance
        self.enhanced_speed = enhanced_speed
        self.enhanced_altitude = enhanced_altitude
        self.heart_rate = heart_rate
        
