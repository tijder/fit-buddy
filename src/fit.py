# fit.py
#
# Copyright 2023 Gerben Droogers
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# SPDX-License-Identifier: GPL-3.0-or-later

import logging
import glob
import math

from garmin_fit_sdk import Decoder, Stream
from .activity import Activity
from .activity_position import ActivityPosition
from .device import Device

class Fit():
    __path = None

    def __init__(self, path):
        self.__path = path

    def device(self):
        messages = self.__parse(self.__path + "/Device.fit")
        device = Device(garmin_product=messages['file_id_mesgs'][0]['garmin_product'], device_type=messages['file_id_mesgs'][0]['type'], manufacturer=messages['file_id_mesgs'][0]['manufacturer'], serial_number=messages['file_id_mesgs'][0]['serial_number'], software_version=messages['file_creator_mesgs'][0]['software_version'])
        logging.info("Parsed device: %s [%i]", device.garmin_product, device.serial_number)
        return device

    def activities(self):
        activities = [self.activity(f) for f in glob.glob(self.__path + "/Activity/*.fit")]
        print(activities)
        return activities

    def activity(self, file: str) -> Activity:
        messages = self.__parse(file)
        activity = Activity(messages)
        for record in messages['record_mesgs']:
            if 'position_lat' in record:
                activity.add_position(ActivityPosition(timestamp=record['timestamp'], position=[record['position_lat'] *(180/math.pow(2,31)), record['position_long'] *(180/math.pow(2,31))], distance=record['distance'], enhanced_speed=record['enhanced_speed'], enhanced_altitude=record['enhanced_altitude'], heart_rate=record['heart_rate']))
        logging.info("Parsed activity: %s", activity.sport_name)
        return activity

    def __parse(self, path):
        logging.debug("Parsing: %s", path)
        stream = Stream.from_file(path)
        decoder = Decoder(stream)
        messages, errors = decoder.read(convert_types_to_strings = True, expand_sub_fields = True, expand_components = True,)
        if errors:
            logging.error(errors)
        # logging.debug(messages)
        return messages

