# activity_list_item.py
#
# Copyright 2023 Gerben Droogers
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# SPDX-License-Identifier: GPL-3.0-or-later

import logging

from gi.repository import Adw
from gi.repository import Gtk
from .fit import Fit

@Gtk.Template(resource_path='/nl/g4d/Fitbuddy/activity_list_item.ui')
class ActivityListItem(Gtk.Box):
    __gtype_name__ = 'ActivityListItem'

    _name = Gtk.Template.Child()
    _distance = Gtk.Template.Child()
    _time = Gtk.Template.Child()

    _activity = None

    def __init__(self, activity, **kwargs):
        super().__init__(**kwargs)

        self._activity = activity

        self._name.set_label(activity.sport_name)
        self._distance.set_label("Afstand: {} kilometer".format(round(activity.total_distance/1000, 2)))
        self._time.set_label("Tijd: {} minuten".format(round(activity.total_elapsed_time/60)))

    def get_activity(self):
        return self._activity

