# activity_view.py
#
# Copyright 2023 Gerben Droogers
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# SPDX-License-Identifier: GPL-3.0-or-later

import logging

from gi.repository import Adw, Gtk, GLib
from .fit import Fit

@Gtk.Template(resource_path='/nl/g4d/Fitbuddy/activity_view.ui')
class ActivityView(Adw.Bin):
    __gtype_name__ = 'ActivityView'

    _status_page = Gtk.Template.Child()

    _sport_name = Gtk.Template.Child()
    _sport_sport = Gtk.Template.Child()
    _start_time = Gtk.Template.Child()
    _start_position = Gtk.Template.Child()
    _total_elapsed_time = Gtk.Template.Child()
    _total_distance = Gtk.Template.Child()

    _activity = None
    _file_dialog = None

    def __init__(self, activity, **kwargs):
        super().__init__(**kwargs)
        self._activity = activity
        self._status_page.set_title(activity.sport_name)

        self._sport_name.set_title(activity.sport_name)
        self._sport_sport.set_title(activity.sport_sport)
        self._start_time.set_title(activity.start_time.strftime('%Y-%m-%dT%H:%M:%SZ'))
        self._start_position.set_title(str(activity.start_position[0]))
        self._total_elapsed_time.set_title("{} minuten".format(round(activity.total_elapsed_time/60)))
        self._total_distance.set_title("{} kilometer".format(round(activity.total_distance/1000, 2)))

    @Gtk.Template.Callback()
    def on_download_gpx(self, row):
        logging.debug("On download gpx clicked for: %s", self._activity.sport_name)
        self.__file_dialog()

    def __file_dialog(self):
        self._file_dialog = Gtk.FileDialog()
        self._file_dialog.set_modal(True)
        self._file_dialog.set_title("Where to save gpx file")
        self._file_dialog.set_initial_name("gpx.gpx")
        self._file_dialog.save(callback=self.__file_selected, parent=self.get_root())

    def __file_selected(self, result, data):
        try:
            path = self._file_dialog.save_finish(data).get_path()
            logging.info("Saving gpx file to: %s", path)
            fp = open(path, 'w')
            fp.write(self._activity.get_gpx_track())
            fp.close()
        except GLib.GError as e:
            logging.info(e)

