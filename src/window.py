# window.py
#
# Copyright 2023 Gerben Droogers
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# SPDX-License-Identifier: GPL-3.0-or-later

import logging

from gi.repository import Adw
from gi.repository import Gtk

from .activity_view import ActivityView
from .device_view import DeviceView
from .fit import Fit


@Gtk.Template(resource_path='/nl/g4d/Fitbuddy/window.ui')
class FitbuddyWindow(Adw.ApplicationWindow):
    __gtype_name__ = 'FitbuddyWindow'

    bin = Gtk.Template.Child()
    leaflet = Gtk.Template.Child()

    _child = None

    def __init__(self, **kwargs):
        super().__init__(**kwargs)

        logging.basicConfig(level=logging.DEBUG, format='%(asctime)s - %(levelname)s - %(filename)s:%(funcName)s:%(lineno)d - %(message)s')
        fit = Fit("/home/gerben/Documenten/GARMIN/")
        deviceView = DeviceView(fit)
        self.bin.set_child(deviceView)
        deviceView.connect("activity-selected", self.__on_activity_selected)

    def __on_activity_selected(self, widget, activity):
        logging.info("Navigate to: %s", activity)
        statusPage = ActivityView(activity)
        if self._child != None:
            self.leaflet.remove(self._child.get_child())
        self._child = self.leaflet.append(statusPage)
        self.leaflet.navigate(Adw.NavigationDirection.FORWARD)

    @Gtk.Template.Callback()
    def on_navigate_back(self, widget):
        self.leaflet.navigate(Adw.NavigationDirection.BACK)

    @Gtk.Template.Callback()
    def on_navigate_forward(self, widget):
        self.leaflet.navigate(Adw.NavigationDirection.FORWARD)
